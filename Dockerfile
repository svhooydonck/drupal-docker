FROM drupal

#Fix for openshift root/port binding
RUN sed -i -e 's/Listen 80/Listen 8080/g' /etc/apache2/ports.conf
RUN chmod -R 777 /var/lock/apache2
RUN chmod -R 777 /var/run/apache2

#Fix for owner of folder
#COPY apache2-foreground /usr/local/bin
#RUN chmod +x /usr/local/bin/apache2-foreground && chmod a+w /etc && chmod a+rw /etc/passwd

#bad fix
RUN chmod -R 777 /var/www/html/modules && chmod -R 777 /var/www/html/sites && chmod -R 777 /var/www/html/themes

EXPOSE 8080
